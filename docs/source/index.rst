Python Datetime Machine
=======================

Contents:

.. toctree::
   :maxdepth: 5

    Introduction <introduction>
    Constants <constants>
    Variables <variables>
    Utilities <utils>
    Library <library>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

